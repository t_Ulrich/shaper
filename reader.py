def readInstructions(filename):

    file = open(filename, 'r')
    lines = file.readlines()
    file.close()

    instruction_list=[]

    for line in lines:
        words=line.split(' , ')
        instruction_words=[]
        for word in words:
            clean_word = word.strip()
            instruction_words.append(clean_word)
        instruction_list.append(instruction_words)

    return instruction_list
