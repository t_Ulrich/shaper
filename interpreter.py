from justshaper import drawShape

def interpretInstructions(instruction_list):
  for instruction_tokens in instruction_list:
    instruction = instruction_tokens[0]

    if instruction[0] == "#":
        pass
    elif instruction[0] == "SHAPER":
        drawShape(instruction[1], instruction[2], instruction[3])
